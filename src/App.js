import { Container, Row, Col } from 'react-bootstrap';
import NavMenu from './Componants/NavMenu';
import React, { Component } from 'react'
import MyForm from './Componants/MyForm';
import MyTable from './Componants/MyTable';
import './App.css';
export default class App extends Component {
 
  render() {
    return (
      <>
      <NavMenu/>
      
      <Container>
      <Row className = "padding">
      <Col xs={12}>
             <logoLogin/>
        </Col>
        <Col xs={4}>
             <MyForm/>
        </Col>
        <Col xs={8}>
             <MyTable/>
        </Col>
        </Row>
      </Container>
      </>
    )
  }
}
