import React from "react";
import {Table, Form, Row, Button, Col} from "react-bootstrap";
export default function MyTable() {
  return (
    <div>
      <Table striped bordered hover size="sm">
        <thead>
          <tr>
            <th>#</th>
            <th>Username</th>
            <th>Email</th>
            <th>Gender</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Mark</td>
            <td>Otto</td>
            <td>mark@gmail.com</td>
          </tr>
          <tr>
            <td>2</td>
            <td>Dara</td>
            <td>Keo</td>
            <td>keo@gmail.com</td>
          </tr>
          <tr>
            <td>3</td>
            <td>Daro</td>
            <td>Kao</td>
            <td>daro@gmail.com</td>
          </tr>
        </tbody>
      </Table>
      <Form.Group as={Row}>
          <Col sm={12}>
            <Button type="submit" variant="danger">Delete</Button>
          </Col>
        </Form.Group>
    </div>
  );
}
